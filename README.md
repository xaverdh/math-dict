# math-dict

## What it is

This is a simple plain text dictionary for spell checking math documents.

## Usage

Fist clone this repository.
```sh
git clone https://gitlab.com/xaverdh/math-dict
```

As an example, to use this as your personal aspell dict, you would
copy the file *lang*.merged ([en.merged][en-merged-file] say) to the proper location.
On most unix systems that is:
```sh
cd math-dict
echo 'personal_ws-1.1 en '$(wc -l < en.merged) > ~/.aspell.en.pws
cat en.merged >> ~/.aspell.en.pws
```

## Contributing

Contributors add their files in the language specific subdirectories.
These then get merged into a common (per language) output file, which is named *lang*.merged .
If you wish to, you can then add yourself to the [contributors][contributors-list] file.
There is no need to ensure that your file does not contain duplicates or overlaps with
those of other contributors. Such duplicates are removed when merging into the output file.
In fact you can just overwrite the contents of your contributors file with the merged file,
effectively using this as a base for your next additions, if you want to.

To add / remove / correct entries from your (or another contributors) additions,
simply file a pull request for your (his) file. The changes will then be reflected in the
merged file. 

## Legal

*Still to be decided.*

## Contributors

See [contributors][contributors-list] (currently just me).

[contributors-list]: https://gitlab.com/xaverdh/math-dict/blob/master/contributors
[en-merged-file]: https://gitlab.com/xaverdh/math-dict/blob/master/en.merged



